//
//  Entity+CoreDataProperties.swift
//  UISplitViewControllerExample
//
//  Created by Mahesh Kumar on 3/11/16.
//  Copyright © 2016 Ravi Shankar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Entity {

    @NSManaged var title: String?
    @NSManaged var location: String?
    @NSManaged var workType: String?
    @NSManaged var name: String?
    @NSManaged var start: String?
    @NSManaged var end: String?

}

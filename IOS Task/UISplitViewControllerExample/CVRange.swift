//
//  CVRange.swift
//  CVCalendar Demo
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import UIKit

public enum CVRange {
    case Started
    case Changed
    case Ended
}
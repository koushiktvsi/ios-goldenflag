//
//  CVWeekdaySymbolType.swift
//  CVCalendar Demo
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import Foundation

@objc public enum CVWeekdaySymbolType: Int {
    case Normal
    case Short
    case VeryShort
}
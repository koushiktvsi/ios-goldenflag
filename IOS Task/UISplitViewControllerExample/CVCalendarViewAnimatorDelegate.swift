//
//  CVCalendarViewAnimatorDelegate.swift
//  CVCalendar
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import UIKit

@objc
public protocol CVCalendarViewAnimatorDelegate {    
    func selectionAnimation() -> ((DayView, ((Bool) -> ())) -> ())
    func deselectionAnimation() -> ((DayView, ((Bool) -> ())) -> ())
}
//
//  CVCalendarViewPresentationMode.swift
//  CVCalendar Demo
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import UIKit

@objc public enum CVCalendarViewPresentationMode: Int {
    case MonthView
    case WeekView
}
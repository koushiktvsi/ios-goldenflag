//
//  CVScrollDirection.swift
//  CVCalendar Demo
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import Foundation

public enum CVScrollDirection {
    case None
    case Right
    case Left
    
    var description: String {
        get {
            switch self {
            case .Left: return "Left"
            case .Right: return "Right"
            case .None: return "None"
            }
        }
    }
}
//
//  CVCalendarMenuViewDelegate.swift
//  CVCalendar Demo
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import Foundation
import UIKit

@objc
public protocol CVCalendarMenuViewDelegate {
    optional func firstWeekday() -> Weekday
    optional func dayOfWeekTextColor() -> UIColor
    optional func dayOfWeekTextUppercase() -> Bool
    optional func dayOfWeekFont() -> UIFont
    optional func weekdaySymbolType() -> WeekdaySymbolType
}
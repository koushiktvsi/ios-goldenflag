//
//  ViewController.swift
//  UISplitViewControllerExample
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var numberLabel:UILabel?
    

    override func viewDidLoad() {
        super.viewDidLoad()

      if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone{
         let navController = splitViewController?.viewControllers[0] as? UINavigationController
            navController!.popViewControllerAnimated(true)
        }
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}


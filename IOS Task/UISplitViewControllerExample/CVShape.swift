//
//  CVShape.swift
//  CVCalendar Demo
//
//  Created by Admin on 26/02/16.
//  Copyright © 2016 tvsi. All rights reserved.
//

import UIKit

public enum CVShape {
    case LeftFlag
    case RightFlag
    case Circle
    case Rect
}

#### Quality of the code, Research & Design Approach #############
  
- Followed MVC pattern, Delegates.
- Analysed calendar view implementation using swift and loading datas depending upon the dates pressed in the calendar
- Analysed SplitView controller and designed universally to suite both iPhone/iPad based on the master & detail view assumption 
- Questions raised for designing split view controller & communicating with real time datas & Search text placement.
- Designed using Storyboard by adding splitview controller
- Displaying calendar view and table view in master class
- Comments have been provided where ever needed. 

#### Developer's Approach, App's focus ################### 

-  Achieved the exact design, functionality, MVC pattern coding structure.
- Displaying the events depending upon the dates selected by user
- The JSON data considered is dummy one from Github repositories. This can be further refined with proper JSON with date fields.
- Table row is moved just for illustration sake for each date click. This has to be further refined with proper date field in the JSON array considered.
- Developed the app in Swift, without using any Objective-C classes
- Used sample API for storing and displaying the datas
- Used Calender View supporting files in swift
- Implementing SplitView controller for iPad/iPhone Using Auto Layout Concept

###### If More time had been provided ###################
 
- More illustration of dealing with real time data.    
- Clear JSON dummy data
- Better graphics in the UI provided.
- Detailed view for Ipad would have been filled with data.
- Actual filtering based on the "All & My events" category would have been done by considering dummy data for categories.
- Further unit testing.
- Cocoa pods has not been used. We can figure out a actual need and implement pods for the application.